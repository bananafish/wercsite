Got a new box from ramnode? For some reason their Debian images are filled with services you don't need!

This is how I prep my ramnode boxes




<pre>
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get install syslog-ng postfix htop fail2ban -y
# for my ansible use (you don't need this)
apt-get install python-apt aptitude curl
apt-get remove --purge samba apache2
update-rc.d -f apache2 remove
reboot
</pre>


