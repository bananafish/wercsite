Archive-name: sysadmin-recovery
Posting-frequency: monthly
Version: 1.799999999999999998... (1 April 1974)
URL: ftp://rtfm.mit.edu/pub/faqs/sysadmin-recovery

alt.sysadmin.recovery FAQ v1.799999999999999998... (1 April 1974)

Happy fucking new year.

This is the frequently-asked questions list for alt.sysadmin.recovery,
a newsgroup for practising and recovering system administrators.

1) ABOUT THE NEWSGROUP

1.1) What is alt.sysadmin.recovery?

1.2) Special note RE: alt.humor.best-of-usenet

1.3) What is not welcome on alt.sysadmin.recovery?

1.4) What does BOFH mean?  How about LART?  TTTSNBN?  PFY?  Cow-orker?

1.5) Official ASR mottos

1.6) If you find sysadminning to be such stress, why not find a job other than being a sysadmin?

1.7) What is the scary devil monastery?

1.8) I hate this group!  Where do I complain?

1.9) Why is this FAQ dated April 2006 even though it has been updated more recently than that?

1.10) So when WAS this FAQ last updated?

2) ABOUT OUR FINE PROFESSION
2.1) I want to be a sysadmin.  What should I do?
2.2) So, I've just "volunteered" to be a sysadmin.  What do I do?
2.3) Where do sysadmins rank as a profession?
2.4) What's a typical day in the life of a sysadmin?
2.5) Do sysadmins all drink a lot?
2.6) Why can't I find my sysadmin?

3) OUR LITTLE FRIEND, THE COMPUTER
3.1) Are there any OSes that don't suck?
3.2) Are there any vendors that don't suck?
3.3) How about any hardware?
3.4) Just HOW MUCH does this system suck?
3.5) Where can I find clueful tech support?
3.6) What can I do to help my computers behave?

4) OUR BIG HEADACHE, THE LUSERS
4.1) I'm on tech support.  Where can I find clueful customers?
4.2) Some tips for general luser interaction
4.3) What is the best way to deal with lusers?
4.4) Revolvers, cyanide and high voltages:  The pros and cons of various luser
education strategies.
4.5) How can I clean up the mess made by a luser's brain splattered across a
monitor?
4.6) What is the penalty for murdering a luser?
4.7) How much should I charge for holding their hands?

5) DEALING WITH BEING A SYSADMIN
5.1) Caffeine and other recreational pharmaceuticals
5.2) The ASR Drinking Game
5.3) The excuse server
5.4) The insults server
5.5) Should I slit my wrists across or downward?
5.6) Sysadmin tools
5.7) Psychiatric assistance
5.8) But seriously, should I kill myself?

6) OK, SERIOUSLY FOLKS!  HELP!!!1!
6.1) I wouldn't ordinarily do this, but I _need_ to ask a technical question.
6.2) You guys are all meanies/elitist/a bad example/corrupting/fattening
6.3) DOODZ!!!!!! W3R3 CAN 1 F1ND SUM K3WL WAREZ???????????
6.4) Does Network Solutions suck?

7) OTHER RESOURCES
7.1) Other newsgroups
7.2) Where can I read about the BOFH?
7.3) ASR mailing lists
7.4) What does MCSE stand for?
7.5) The Coat of Arms
7.6) Where is Spike Bike (http://www.chiark.greenend.org.uk/~mjh/spike.html)
mentioned in the ASR FAQ?


------------------------------

1) ABOUT THE NEWSGROUP

1.1) What is alt.sysadmin.recovery?

Alt.sysadmin.recovery is a usenet newsgroup for discussion by recovering
sysadmins, recovered sysadmins, and sysadmins who can but yearn for recovery
some fine day.  It is a forum for mutual support and griping over phenomenally
stupid users, mind-bogglingly unhelpful tech support, surprisingly lousy
software, astonishingly deficient hardware, and generally how idiotic this
job is.  Think of it as a virtual pub where we can all go after hours to
gripe about our job.  Since the concept of "after hours" is unknown to your
average sysadmin, we have this instead.

Warning: If you are a user, you may well see your sysadmin posting messages
about how stupid YOU are.  (But none of us will be surprised when you fail
to take heed of this warning.)

1.2) Special note RE: alt.humor.best-of-usenet

It is prohibited to re-post alt.sysadmin.recovery messages to
alt.humor.best-of-usenet.  Most ASR denizens have nothing against that group
itself, but sometimes in the past we have averaged a few messages a day there.
This has drawn the lusers here like moths to a candle -- more unpleasant
for the moths than for the candle, but we don't care about the moths.
We strongly recommend that you put "X-No-Ahbou: yes" in your headers.

1.3) What is not welcome on alt.sysadmin.recovery?

If many people object to your articles, consider the possibility that it
might be you.  Some people are just a waste of skin.

But occasionally it will be the topic, rather than the user.
Not welcome on alt.sysadmin.recovery are:  advocacy, user questions,
users (unless you are a sysadmin in another life), amateur salespeople, or
general cluelessness.  Particularly unwelcome is ANY real sysadmin related
stuff.  Useful (to our job) information is forbidden.  It doesn't matter if
you ROT-13 it, cast it as a sonnet, or attach it as a mime-encoded mpeg,
just don't do it.  We are here to escape, and there are plenty of other
newsgroups that will be able to answer your question.  And please avoid
excessive quoting, "me toos", etc.

There are things that have become tedious.  Gratuitous ROT-13 (especially
frequent switching on and off within a message), $USE $OF $SHELL $VARIABLES,
ranting about it (a single, well-aimed "BLAM" will suffice when necessary),
and anything else the Official ASR Taste Committee deems tired.  Yes,
you are smart, young, old, had trouble in school, enjoyed school, never
went to college, use vi, use emacs, hate them both, read Robert Heinlein,
have a cool web page, and practice martial and marital arts.  Please don't
tell us about it.

To repeat, ROT-13 does not make a posting acceptable.  It can, however,
interfere with luser searches.  Sometimes we rotate text to avoid lusers who
might search for their own name or their company name, and we certainly rotate
common luser-magnet software names such as yvahk.  (There is still a debate
about whether or not you have to rotate the 'Q' word, but indeed the article
http://groups.google.com/groups?selm=97ltlp%24s21%241%40atlas.dgp.toronto.edu
did not cause untoward summoning.)

Most of all:  DO NOT mention how to post here, don't even hint about it,
and don't tell them or hint about it by e-mail.  It's already far too
easy to work it out.  Those who belong here can figure out how to post.
Unfortunately, so can several people who don't belong here.  Fortunately,
we usually manage to make such posters leave "willingly".

By the way, don't cross-post to newsgroups with different moderation policies
(e.g. unmoderated groups).  It accomplishes nothing and annoys the pig, or
something like that.

1.4) What does BOFH mean?  How about LART?  TTTSNBN?  PFY?  Cow-orker?

BOFH:  Bastard Operator From Hell.  Our role model.
    (The Bastard Operator From Hell was originally a series of stories
    written by Simon Travaglia, s.travaglia@waikato.ac.nz.  See section 7.)

LART:  Luser Attitude Readjustment Tool.  Something large, heavy, and painful.

TTTSNBN:  [sorry, I can't name it here]

PFY:  A term for a junior sysadmin indicating both age and skin condition,
    both often metaphoric.

Cow-orker:  A one-time misplaced hyphen (in alt.folklore.urban, in fact)
    revealed a shameful truth here.

Copro-grammer:  literally, "writer of feces"

C|N>K:  C is coffee, N is nose, K is keyboard; and think unix shell syntax.

STR:  synchronous transmit receive.  A mode of the synchronous communications
    adapter that provides for point-to-point operation only.  (Rumour has it
    that 'S' actually secretly stands for "slurp".)

1.5) Official ASR mottos

The official ASR motto, our catch phrase, is the immortal:

	"Down, not Across"

It is our mantra.  We recite it to ourselves as we deal with the day-to-day
realities of a life that is far more nasty, brutish, and short than even
Hobbes could have ever imagined.

Some other mottos include:
	*clickety-click*
	"Oh, lovely."
	"Just DON'T."
	"I need a drink."
	"Either way I'm screwed."
	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

The official ASR position:
	Hiding in a corner, under a desk, in fetal position, arms covering
	head and quietly whimpering.

The official ASR luser position:
	6 feet under.

1.6) If you find sysadminning to be such stress, why not find a job other than
being a sysadmin?

	"Why not?  Sure, why not?  Why not just kick the heroin habit?
	 Why not just stop breathing if the air gets polluted?"

Sysadmins are driven by a desire to _make_it_work_.  We loathe non-functioning
pieces of crap.  Similarly, we hate seeing equipment working at substantially
below its potential due to moronic admin decisions.

Doesn't this mean we should shun sysadminning, then?  Well, as you will
see from discussion in ASR, we have this tendency to drift into the job.
It starts when you're given a machine on your desk which crashes every
five minutes, and you know how to make it crash only once every five days.
You get given the root/administrator password.  You end up fixing someone
else's machine too.  As word gets out, you have more and more people
calling on you for basic computer administration assistance every week.
Eventually you get told that your sysadmin work is more important than
what you were doing before.  Since you seem to be the only person in your
company who is doing any work, you have to agree.

After a while, you quit and go to work for somewhere else, where you're
promised you won't have to be a sysadmin.
But the machine on your desk crashes every five minutes...

1.7) What is the scary devil monastery?

alt sysadmin recovery
rancid mystery loaves
steady micron slavery
comedy striven salary
trendy mosaic slavery
convert already missy
scary devil monastery
misty adversary clone
discover anal mystery

1.8) I hate this group!  Where do I complain?

Probably you want the folks who run the net.  Address mail to:

    Usenet Central Administration
    1060 West Addison Street
    Chicago, Illinois
    USA 60613-4305

Allow six to eight weeks for a response.

1.9) Why is this FAQ dated April 2006 even though it has been updated more
recently than that?

Ha ha, April Fools!

1.10) So when WAS this FAQ last updated?

If you have to ask, you can't afford it.


------------------------------

2) ABOUT OUR FINE PROFESSION

2.1) I want to be a sysadmin.  What should I do?

Seek professional help.

2.2) So, I've just "volunteered" to be a sysadmin.  What do I do?

See 5.4.

2.3) Where do sysadmins rank as a profession?

Apparently held in considerably lesser regard than building maintenance,
because no one goes complaining to the janitors saying that it's the janitors'
fault that they can't manage to throw garbage into their garbage bin from
across the room.

2.4) What's a typical day in the life of a sysadmin?

Perhaps Abby Franquemont summarized the life of a sysadmin the best,
when she described us as:

"disgruntled, disenchanted with things we used to really get a kick out of,
foul tempered, hard-drinking, heavy-smoking, overworked, with no real social
life to speak of."

Or perhaps she was being optimistic.

2.5) Do sysadmins all drink a lot?

Most sysadmins seem to, but there are always exceptions.
In particular, some psychotropic medication makes you react very strongly
to alcohol; if you're on such medication, you find that you can't _ever_
have dozens of drinks in an evening, and if you have five drinks in an
evening you'll feel like you used to when you occasionally had dozens.
Then as you drink less your tolerance decreases, and pretty soon one drink
will make you too drunk to go back to work after lunch.  All this is to say
that psychotropic medication (e.g. SSRIs) seems to be either software or
hardware, because it sucks.  And for some reason, a lot of sysadmins seem
to need medication.  See section 5.

2.6) Why can't I find my sysadmin?

Would you want to be found by a luser?


------------------------------

3) OUR LITTLE FRIEND, THE COMPUTER

3.1) Are there any OSes that don't suck?

No.  See http://linuxmafia.com/cabal/os-suck.html

3.2) Are there any vendors that don't suck?

No.

3.3) How about any hardware?

The PDP-10 was pretty nice.  Pity they aren't made any more.

3.4) Just HOW MUCH does this system suck?

The ASR standard unit of suckiness is the Lovelace (Ll).
This is defined as:  One Lovelace is the amount of force (measured in dynes)
it takes to draw a round ball weighing e Troy Ounces down a tube it fits
exactly (in air) at a speed of pi attoparsecs/microfortnight.

Like Farads, this is a rather large measurement.  Thus, Plan 9 sucks a few
mLl, for instance, while your average Microsoft product achieves many Ll.

3.5) Where can I find clueful tech support?

HAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHAHA

There is a device the telco puts on the phone that ensures that whoever is
on the other end of a service call is always a drooling moron with the IQ
of a potted plant.  Note that this applies both ways (see 4.1).

3.6) What can I do to help my computers behave?

Some go for the carrot approach, others the stick, others both.  If you
favour the carrot, try offering memory upgrades or faster processors.
For sticks, try bullwhips.  Many computers are easily fooled, so placing
a picture of yourself in front of a computer will often cause it to think
you are watching and it will be too scared to misbehave.

Remember that if computers are networked, they can talk to each other.
That is useful in that you can make an example of one and the others will
watch (and hopefully learn).

Of course, some computers require a blood sacrifice.  But you don't have to
worry about this one, because they will simply take the blood without asking.


------------------------------

4) OUR BIG HEADACHE, THE LUSERS

4.1) I'm on tech support.  Where can I find clueful customers?

See item 3.5.  A clueful luser is an oxymoron.

4.2) Some tips for general luser interaction

- lusers, bless their little hearts, have simple minds.  Even if you
  think that a lobotomized flatworm could understand your instructions,
  your luser probably won't.  And we tried lobotomizing a few lusers to
  see if it would help, but it didn't make any detectable difference.

- since lusers will neither read nor understand any docs you write for them,
  just don't even bother.

- NEVER anthropomorphize lusers.

4.3) What is the best way to deal with lusers?

Lusers are much easier to deal with if they aren't breathing.  240V across the
heart, a revolver round through the head, or even a simple little broadsword
thrust into their abdomen will improve your interactions wonderfully.
See next item.

4.4) Revolvers, cyanide and high voltages:  The pros and cons of various luser
education strategies.

There has been a great deal of debate on ASR about the best way of dealing
with lusers, and at this time no consensus has been reached.  What we can
suggest, however, is to be sure it is painful, clean, and doesn't harm
the computer.  That unfortunately leaves a lot of options out -- you can't
just throw a grenade at them; it will hurt the machine.

4.5) How can I clean up the mess made by a luser's brain splattered across a
monitor?

First of all, be careful.  While cluelessness is not contagious, there are
some nasty things that can be picked up from lusers, such as blood-transmitted
diseases.  (Watch out for Creutzfeldt-Jacob syndrome, aka mad cow disease.
Of course, we are all mad anyway.)  Be sure to wear latex gloves --
available for 40 cents a pair 'round here -- when you're through, remove
them by peeling them off carefully so that they end up inside-out, and
discard.

Luser guts will usually clean up with soap and water.  They say that
to clean up blood you should use cold water, not warm; a little bleach
sometimes helps for the more stubborn cases.  Be sure to get all of the
blood off the keyboards or the keys may get very sticky.

To get rid of the body, people have suggested using several garbage bags
and a large quantity of duct tape.  If you have to keep it for a while,
try to remove the guts; that will keep the smell down.  Alcohol and formalin
works well as a preservative.

4.6) What is the penalty for murdering a luser?

Unfortunately, in the eyes of the law, lusers are treated like humans.  We
therefore recommend you be discreet in your luser era^H^Hducation campaigns.

4.7) How much should I charge for holding their hands?

See the official ASR price list at
http://unix.rulez.org/~calver/funny/pricelist.html

Warning: Like their computers, live lusers are usually infected with viruses.
Either reformat ^W kill them first or use standard biohazard precautions.


------------------------------

5) DEALING WITH BEING A SYSADMIN

5.1) Caffeine and other recreational pharmaceuticals

Caffeine: Much information about this can be found on alt.drugs.caffeine.
See FAQs at http://db.uwaterloo.ca/~alopez-o/caffaq.html

Booze: Much low-quality information around the net these days.  Perhaps try
rec.food.drink.*, alt.drunken.bastards, http://www.beerinfo.com, and/or
http://www.camra.org.uk

Other: rec.drugs.*, http://www.lycaeum.org

5.2) The ASR Drinking Game

The rules for the ASR Drinking Game, as hashed out over a period of several
decades ending on 27 January 2007, are quite simple.  When something happens,
drink.  As much as necessary.

5.3) The excuse server

This is an important net resource that lets you give the exact reason why
you can't do something yesterday.
http://www.cs.wisc.edu/~ballard/bofh/
Or telnet bofh.jeffballard.us 666
Or telnet bofh.ucc.asn.au 666

5.4) The insults server

Once you have got rid of your lusers, you will probably want to tell them
what they really are.  For this, the insults server is useful.
telnet insulthost.colorado.edu 1695

5.5) Should I slit my wrists across or downward?

Downward.

I don't see any reason to cite http://www.depressed.net/suicide/suicidefaq.txt
here, but some people think I should.

5.6) Sysadmin tools

There are a number of tools important to system administration.  Most
important are chemical by nature; see item 5.1.  A particularly useful
tool, revered by many of ASR, is the noble chocolate-covered coffee bean.
Peter Corlett reports that they can also be bought from Cranberry (stalls
dotted around London and also rather more sparsely around the rest of the
UK), and he has also seen fancily-packed versions at fancy prices in the
food courts of the likes of House of Fraser and Selfridges as well as common
or garden supermarkets.

You can also make your own.  Melt some chocolate, place some coffee beans
in it, and Bjorn Stronginthearm's your uncle!

Next in line is a good LART.  A 2x4 works fine, but a real professional needs
something a little more effective.  Unfortunately, this is a very personal
thing, and no consensus has yet been reached on the group.  Everything from
a simple 7.65mm Walther (for the Bond fans only; it's not a very good gun)
to a 155mm with depleted Uranium rounds has been suggested, some even going
for exotic things like Thermite, nukes, or flamethrowers.  For further info,
look at the rec.guns home page (http://recguns.com).

You can find a lot of cool stuff at Military Surplus stores.  Sadly, they
don't sell the _really_ interesting surplus stuff like tanks or F16s.
Try US Cavalry, 1-800-777-7732, or http://www.uscav.com.

When you can't use the LART (e.g. you don't want to damage a computer),
Nerfs are excellent substitutes.  These are a range of foam weapons.

The leatherman is another useful tool.  The Perl of Swiss Army Knives,
this shouldn't be too hard to find.

Finally, there are some tools a sysadmin is forbidden from having.  Adequate
computing power is first on this list, but the most important is called a
"life".  And be warned: life is always eventually fatal.

5.7) Psychiatric assistance

If you are reading this, you need it.  Contact your family doctor.
Perhaps consider http://www.aspergersyndrome.org.

Medication for depression and/or OCD these days is often an "SSRI",
a category of drug which changes your brain chemistry not subtly.
SSRIs tend to have a range of seemingly unrelated side effects, such as
decreased alcohol tolerance, difficulty reaching orgasm (depending upon
dosage and other factors), weight gain, intestinal changes.  But despite
the drawbacks, some people report advantages such as greater stability,
e.g. the ability to refrain from choking the living shit out of a luser who
richly deserves it, and, occasionally, enjoyment of life.  See a pshrink near
you to determine whether you could benefit from psychotropic medication.
Some of the checklists on the web aren't complete bullshit, but keep in
mind that Sturgeon's law was an underestimate for anything important.

5.8) But seriously, should I kill myself?

Seriously, no.
As posted to ASR by Ed Evans:

	Ultimate recovery stalks us all, no need to succour it.  Quit or
	take a leave with or without pay (or permission), stop seeing him
	or her, recognise that the cat or dog does rule you, call in sick
	and spend the day in the big blue room, it's only money and can
	be earned again, all the pictures will be posted again, call the
	local professionals if you really feel that way...

	And if all else fails?	Lawn mowing.

In chess they have a saying, "You can't win by resigning."  If you're willing
to take the severe step of killing yourself, you could consider less severe
steps such as quitting your job, or cutting out other distasteful parts of
your life without permission.

And more of us have been there than you may realize.  If your life sucks,
the tendency for "regression toward the mean" means that things are very
likely to improve in the future.  Suffer through the next year or two
without doing any permanent damage, and things will look up.  Today is
an insignificant fraction of the rest of your life; it's not all there is.


------------------------------

6) OK, SERIOUSLY FOLKS!  HELP!!!1!

6.1) I wouldn't ordinarily do this, but I _need_ to ask a technical question.

Fuck off.  Really.  See item 1.3 above.

6.2) You guys are all meanies/elitist/a bad example/corrupting/fattening

Yep!

6.3) DOODZ!!!!!! W3R3 CAN 1 F1ND SUM K3WL WAREZ???????????

Sounds like you want the Warez-net.  Here are a list of some participating
sites:
warez.satanic.org
warez.phantom.com
warez.plethora.net
warez.terra.nu
warez.rtfm.net

These places also have a good collection of gifs.
(You need to log in with your own name and password)
For more information, look at the Warez-Net home page at
http://www.bofh.net/~koos/warez.html

6.4) Does Network Solutions suck?

Yes.


------------------------------

7) OTHER RESOURCES

7.1) Other newsgroups

alt.folklore.computers
    I've heard that this newsgroup is back in shape, but I haven't had the
    chance to check it out yet.  There _are_ lusers there battling over toy
    OSes, and I've heard that a kill file is essential.

The jargon file
    Not _particularly_ sysadmin related, this is rather a document about
    general computer lore.  It may be found at http://www.catb.org/jargon/

alt.fan.pratchett
    For some reason, there seems to be a large amount of synergy between the
    followers of the eternal Mr Pratchett and ourselves.  You be the judge.

alt.fan.mailer-daemon
    "a hilariously-poorly propagated newsgroup that hardly anybody is ever
    likely to read"

Various cartoons
    Many people in ASR like certain lame cartoons like User Friendly.
    I don't.  Anyway, you can find them on the net if you have the brains
    given to a small slug.  Thus even some of your users can find them...

alt.suicide.holiday
    Name says it all, really.

7.2) Where can I read about the BOFH?

http://bofh.ntk.net/BOFH/
Newer adventures may be found at the register, http://www.theregister.co.uk

7.3) ASR mailing lists

There are a number of local ASR mailing lists dealing with organizing
local ASR gatherings.  They include:

ASR New York:
To subscribe:  danj@3skel.com

ASR Boston:
To subscribe:  lists@mathworks.com (it's a majordomo list, name is asr-boston)

ASR London:
To subscribe:  asrlon-request@bofh.org.uk

ASR Israel:
To subscribe:  asr-il-request@cs.huji.ac.il

The Twin Cities [1]:
List address:  asrmeet@winternet.com

ASR DC:
To subscribe:  majordomo@lists.netset.com
               Listname is "asr-dc"

--
[1] *Which* twin cities, you ask?  I could tell you, but then Matthew Crosby
would have to kill you.  Or perhaps me.
--

7.4) What does MCSE stand for?

Lots of things.  "Must Consult Someone Experienced" is a good one.  See
http://www.leftmind.net/asr/mcse.txt for more.

7.5) The Coat of Arms

While the College of Arms are still sitting on our application (bastards...
I think a disk crash is in order), you can see the potential ASR coat of
arms at http://www.cs.umanitoba.ca/~djc/asr/

7.6) Where is Spike Bike (http://www.chiark.greenend.org.uk/~mjh/spike.html)
mentioned in the ASR FAQ?

In section 7.6.

