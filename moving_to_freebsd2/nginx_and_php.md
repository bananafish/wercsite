Okay for my ususal "LAMP" stack I use NGINX instead of apache and php-fpm for php processing for NGINX. For just about all of my servers I need a basic nginx setup. I will explain how to do this as follows:

Install NGINX from ports:

	cd /usr/ports/www/nginx
	make install clean

That will go for a little while and ask you several questions on what options and features you will want for your NGINX install. Pay attention to these options otherwise you will have to recompile NGINX when you realize you need REWRITE support or something.

Next we need to install PHP-FPM, this does the processing of PHP files for NGINX. This is definetly more involved than installing php5-fpm on debian.

First we need to install libtool, it's a a generic library support script. Libtool hides the complexity of using shared libraries behind a consistent, portable interface. So...

	cd /usr/ports/devel/libtool 
	make install clean

Then we need to install the PHP language, we need to make sure we select FPM here so that it builds what we need.

	cd /usr/ports/lang/php5
	make install clean

Now all the software is created and almost ready to run, we just need to put in our configs.

Nginx config, place this in /usr/local/etc/nginx/nginx.conf , if there is one already there back it up and replace with the following:

<pre>
user www www;
worker_processes 4;
error_log /var/log/nginx/error.log crit;
pid /var/run/nginx.pid;
events {
    worker_connections 1024;
}
http {
    include /usr/local/etc/nginx/mime.types;
    default_type application/octet-stream;
    access_log off;
    server_tokens off;
    sendfile on;
    client_max_body_size 200m;
    client_body_buffer_size 1m;
    keepalive_timeout 1;
    port_in_redirect off;
    gzip on;
    gzip_http_version 1.1;
    gzip_vary on;
    gzip_comp_level 6;
    gzip_proxied any;
    gzip_types text/plain text/css application/json application/x-javascript application/xml application/xml+rss text/javascript;
    gzip_buffers 16 8k;
    gzip_disable "MSIE [1-6].(?!.*SV1)";
    include /usr/local/etc/nginx/conf.d/*.conf;
}
</pre>

Then we need to create some directories

```
mkdir /var/log/nginx
mkdir /usr/local/etc/nginx/conf.d
mkdir /var/www/yourdomain.com
```

Next we need to put in our config for our specific site for nginx, the following is a basic nginx vhost with php support. Replace youdomain.com with the site you are setting up. This goes in /usr/local/etc/nginx/conf.d or wherever you specified in the config earlier.

```
server {
        listen 80;
        server_name yourdomain.com;
        root /var/www/yourdomain.com;

        location / {
            index index.html index.php;
        }
        location ~ \.php$ {
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME /var/www/yourdomain.com$fastcgi_script_name;
            include fastcgi_params;
        }
        location ~ /\.ht {
            deny all;
        }
}
```

Finally we can start up php-fpm and NGINX

```
/usr/local/etc/rc.d/php-fpm onestart
/usr/local/etc/rc.d/nginx onestart
```

Now if we want these services to start with the server we need to add to `/etc/rc.conf` the following:

```
php_fpm_enable="YES"
nginx_enable="YES"
```
